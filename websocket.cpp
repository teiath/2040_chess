// Βοηθητικές συναρτήσεις για την αποστολή μηνυμάτων σε συγκεκριμένο
// websocket, το οποίο επιτρέπει την θέαση της εξέλιξης της παρτίδας
// μέσω ιστοσελίδας (διαδικτυακώς)

#include <string>
#ifdef HAVE_WEBSOCKETS

#define URI "ws://connect.websocket.in/Fo0bJDymOMO8rCnK"
#include "easywsclient/easywsclient.hpp.inc"
#include "easywsclient/easywsclient.cpp.inc"
#include <assert.h>

using easywsclient::WebSocket;
void websocket_send(std::string message)
{
   static WebSocket::pointer ws = NULL;
   ws = WebSocket::from_url(URI);
   assert(ws);
   ws->send(message);
   ws->poll();          // wait until it's sent
   delete ws;
}

#else	// HAVE_WEBSOCKETS
void websocket_send(std::string message){
	// do nothing
}
#endif	// HAVE_WEBSOCKETS

