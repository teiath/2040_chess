#ifndef SHOWMESSAGE_H
#define SHOWMESSAGE_H

#include <string>
using namespace std;

void showMessage(string msg, string fen, string move);
#endif
