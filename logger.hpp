#ifndef LOGGER_H
#define LOGGER_H

#include <fstream>
#include <string>
using namespace std;

class logger
{
private:
  ofstream logstream;		// το κανάλι στο οποίο γίνεται η καταγραφή

public:
  logger ();
  bool lopen (char *filename);	// δημιουργία αρχείου καταγραφής
   ~logger ();			// κλείσιμο αρχείου καταγραφής

  void log (string s);		// κατάγραψε την συμβολοσειρά s με χρονοσήμαντρο
};

#endif
