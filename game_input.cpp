// Ανάγνωση εισόδου παικτών από την κονσόλα

// Αναγνωρίζονται οι ακόλουθες είσοδοι:
//  Bxc2 ή B:c2         Κάποιος αξιωματικός αιχμαλωτίζει το πιόνι στο c2
//  Bc2                 Κάποιος αξιωματικός μετακινείται στο c2
//  c2                  Κάποιο πιόνι μετακινείται στο c2
//  dc2                 Το πιόνι στην στήλη d μετακινείται στο c2
//  Bac2                Ο αξιωματικός στην στήλη a μετακινείται στο c2
//  B4c2                Ο αξιωματικός στην γραμμή 4 μετακινείται στο c2
//  Ba4c2               Ο αξιωματικός στο a4 μετακινείται στο c2
//  e8=Q ή e8Q          Προαγωγή κομματιού σε βασίλισσα
//  0-0 ή O-O ή ΟΟ      Μικρό ροκέ (με τον πύργο του βασιλιά)
//  0-0-0 ή O-O-O ή ΟΟΟ Μεγάλο ροκέ (με τον πύργο της βασίλισσας)
//  Qc2+                Η βασίλισσα μετακινείται στο c2 και δηλώνεται σαχ
//  Qc2#                Η βασίλισσα μετακινείται στο c2 και δηλώνεται ρουα ματ
//  1-0 0-1 ½–½         Τέλος παιγνιδιού με νίκη/ήττα/ισοπαλία

#include <iostream>
#include <string.h>		// strlen()
#include "common.hpp"
using namespace std;

#include <cstdio>



// Συντακτική ανάλυση της συμβολοσειράς inputstr.
// Γίνεται έλεγχος μόνο αν συμφωνεί με τους κανόνες Αλγεβρικού Συμβολισμού.
// ΔΕΝ γίνεται έλεγχος της ορθότητας των κινήσεων επί συγκεκριμένης σκακιέρας
// Επιστρέφει MOVE_ERROR σε περίπτωση σφάλματος, ή θέτει διάφορες σημαίες
// π.χ. MOVE_CHECKMATE MOVE_PROMOTE MOVE_CAPTURE κ.λπ.
int
check_Algebraic_Notation_syntax (char *inputstr,
				 int *fromX, int *fromY, char *fromType,
				 int *toX, int *toY, char *toType)
{
  char c, *s, buf[512], *str = buf;
  int retval;
  // πάρε ένα αντίγραφο της εισόδου του χρήστη
  memcpy (str, inputstr, sizeof (buf) - 1);

  retval = MOVE_NONE;


  if ((strlen (str) == 3) && (str[1] == '-'))
    {
      if ((str[0] == '1') && (str[2] == '0'))
	{
	  retval = MOVE_WINW;	// νίκη των λευκών
	  return retval;
	}
      if ((str[0] == '0') && (str[2] == '1'))
	{
	  retval = MOVE_WINB;	// νίκη των μαύρων
	  return retval;
	}
    }

  if ((strlen(str) == 5) && (str[2] == '-'))   // '½-½' : c2 bd 2d c2 bd
    {
      if (((unsigned char)str[0]==0xc2)&&((unsigned char)str[1]==0xbd)&&((unsigned char)str[3]==0xc2) &&((unsigned char)str[4]==0xbd))
	{
	  retval = MOVE_DRAW;	// ισοπαλία
	  return retval;
	}
    }

  // convert S (springer) to N (knight)
  // convert : (capture) to x (capture)
  // convert 0 (castling) to O (castling)
  s = str;
  while (*s)
    {
      if (*s == KNIGH2)
	{
	  *s = KNIGHT;
	}
      if (*s == CAPTUR2)
	{
	  *s = CAPTURE;
	}
      if (*s == CASTLIN2)
	{
	  *s = CASTLING;
	}
      s++;
    }

  // remove = - (optional promote/castling delimiter symbols)
  s = str;
  while (*s)
    {
      if (*s == PROMOTE || *s == CASTLING_DELIMITER)
	{
	  char *p = (s + 1);
	  while (*p)
	    {
	      *(p - 1) = (*p);
	      *p = '\0';
	      p++;
	    }
	}
      s++;
    }




  // remove x
  s = str;
  while (*s)
    {
      if (*s == CAPTURE)
	{
	  retval |= MOVE_CAPTURE;
	  char *p = (s + 1);
	  while (*p)
	    {
	      *(p - 1) = (*p);
	      *p = '\0';
	      p++;
	    }
	}
      s++;
    }






  // remove final check/checkmate symbol (# or +)
  s = str + strlen (str) - 1;
  c = *s;
  if (c == CHECK)
    {
      retval |= MOVE_CHECK;
      *s = '\0';
    }
  if (c == CHECKMATE)
    {
      retval |= MOVE_CHECKMATE;
      *s = '\0';
    }




// piece type; if missing it's a pawn
  *toType = *fromType;		// εξαίρεση: όταν έχουμε προαγωγή πιονιού
  s = str;
  c = s[0];
  if (c == KING || c == QUEEN || c == BISHOP || c == ROOK || c == KNIGHT)
    {
      *fromType = c;
      str++;
    }
  else
    *fromType = PAWN;



// promotion type (last character is a piece type)
  *toType = *fromType;
  s = str;
  c = s[strlen (s) - 1];
  if (validType (c))
    {
      *toType = c;
      str[strlen (str) - 1] = '\0';
      retval |= MOVE_PROMOTE;
    }



  *fromX = 0;
  *fromY = 0;			// undefined
  *toX = 0;
  *toY = 0;			// undefined



  if (strlen (str) == 2 && (str[0] == 'O') && (str[1] == 'O'))
    {
      retval = MOVE_OO | MOVE_MOVE;	// μικρό ροκέ
      return retval;
    }
  if (strlen (str) == 3 && (str[0] == 'O')
      && (str[1] == 'O') + (str[2] == 'O'))
    {
      retval = MOVE_OOO | MOVE_MOVE;	// μεγάλο ροκέ
      return retval;
    }




  if (strlen (str) == 4)
    {
      *fromX = str[0] - 'a' + 1;
      *fromY = str[1] - '1' + 1;
      str = str + 2;
    }

  if (strlen (str) == 3 && str[0] >= 'a' && str[0] <= 'a' + COLS - 1)
    {
      *fromX = str[0] - 'a' + 1;
      str = str + 1;
    }

  if (strlen (str) == 3 && str[0] >= '1' && str[0] <= '1' + ROWS - 1)
    {
      *fromY = str[0] - '1' + 1;
      str = str + 1;
    }

  if (strlen (str) == 2 && str[0] >= 'a' && str[0] <= 'a' + COLS - 1
      && str[1] >= '1' && str[1] <= '1' + ROWS - 1)
    {
      *toX = str[0] - 'a' + 1;
      *toY = str[1] - '1' + 1;
      retval |= MOVE_MOVE;
    }
  else
    {
      retval = MOVE_ERROR;
    }

  return retval;
}




// Επεξεργασία εισόδου χρήστη. Εκτός των εγκύρων συμβολοσειρών Αλγεβρικού
// Συμβολισμού, δέχεται και τις εντολές:

//  load filename       Ανάγνωση παρτίδας από δυαδικό αρχείο
//  save filename       Αποθήκευση παρτίδας σε δυαδικό αρχείο
//  init FENstring      Αρχικοποίηση παρτίδας με χρήση συμβολισμού FEN
//  quit ή exit         Τερματισμός προγράμματος
//  #<οτιδήποτε>        Η είσοδος αγνοείται (χρησιμοποιείται για σχόλια)

// Επιστρέφει ότι και η check_Algebraic_Notation_syntax()
// (δηλαδή MOVE_ERROR MOVE_CHECKMATE MOVE_PROMOTE MOVE_CAPTURE κ.λπ.)
// και καθορίζει τη θέση και τον τύπο κομματιού για το τετράγωνο-αφετηρία
// (αν προσδιορίζεται από τον χρήστη) καθώς και το τετράγωνο-προορισμός.


int
game_input (string * inputstr, string * extraarg,
	    int *fromX, int *fromY, char *fromType,
	    int *toX, int *toY, char *toType)
{
  string buf = "";
  int retval;
  getline (cin, buf);		// read a line

  *inputstr = buf;

  if (cin.eof () || (buf.substr (0, 4) == "quit")
      || (buf.substr (0, 4) == "exit"))
    {
      return MOVE_QUIT;
    }

  if (buf.substr (0, 5) == "load ")
    {
      *extraarg = buf.substr (5, buf.size ());
      return MOVE_LOAD;
    }
  if (buf.substr (0, 5) == "save ")
    {
      *extraarg = buf.substr (5, buf.size ());
      return MOVE_SAVE;
    }
  if (buf.substr (0, 5) == "init ")
    {
      *extraarg = buf.substr (5, buf.size ());
      return MOVE_INIT;
    }
  if (buf.substr (0, 1) == "#")
    {
      return MOVE_COMMENT;
    }


//printf("check GI 10\n");
  retval =
    check_Algebraic_Notation_syntax ((char *) buf.c_str (), fromX, fromY,
				     fromType, toX, toY, toType);


  // μη έγκυρος αλγεβρικός συμβολισμός κίνησης;
  if (retval & MOVE_ERROR)
    {
      return MOVE_ERROR;
    }
  else
    {
      return retval;
    }

}



#ifdef TEST_UNIT
#include <cstdio>		// printf()

int
main (int argc, char **argv)
{
  int fromX, fromY, toX, toY;
  char fromType, toType;
  int retval, arg = 1;
  string inputstr, s;

  while (true)
    {
      retval = game_input (&inputstr, &s,
			   &fromX, &fromY, &fromType, &toX, &toY, &toType);
      if (retval == MOVE_QUIT ||
          (retval & MOVE_CHECKMATE) ||
          (retval == MOVE_WINW) ||
          (retval == MOVE_WINB) ||
          (retval == MOVE_DRAW)
         )
	{
	  break;
	}
      else
	{
#ifdef DEBUG
	  printf
	    ("input <%s> output <%c %d %d -> %c %d %d>\t\treturn value:%d\textra_string:<%s>\n",
	     inputstr.c_str (), fromType, fromX, fromY, toType, toX, toY, retval, s.c_str());
#endif
	}

    }
}
#endif /* TEST_UNIT */
