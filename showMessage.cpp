// Εμφάνιση εξόδου παιγνιδιού στην κονσόλα
//
// Αν κατά την μεταγλώττιση η μεταβλητή HAVE_WEBSOCKETS έχει ορισθεί,
// τότε η έξοδος εμφανίζεται και σε ιστοσελίδα μέσω της τεχνολογίας websockets


#include <iostream>
#include <string>
using namespace std;


void showMessage(string msg, string fen, string move){
  void websocket_send(string);
  string w = "";
  if (msg != "") { cout << msg; w = "\"message\":\""+msg + "\""; }
  if (w!="" && fen!="") w += ",";
  if (fen != "") { w += "\"FEN\":\""+fen + "\""; } 
  if (w!="" && move!="") w += ",";
  if (move != "") { w += "\"move\":\""+move + "\""; } 
  w = "{" + w + "}";
  websocket_send(w);
}

