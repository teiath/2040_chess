#ifndef BOARD_H
#define BOARD_H

#include <string>
#include "common.hpp"
using namespace std;

class piece;

class board {
  private:
    piece* b[ROWS][COLS];
    // Δείκτες σε δυναμικά δημιουργούμενα κομμάτι
    // που δείχνουν τι υπάρχει σε κάθε τετράγωνο της σκακιέρας.
    // Αν δεν υπάρχει κομμάτι σε κάποιο τετράγωνο, αποθηκεύεται NULL
  
  public:
    board();   // constructor

    void clear();                  // αφαίρεσε τυχόν κομμάτια
    piece* getXY(int, int);        // τι κομμάτι υπάρχει στη στήλη/γραμμή X,Y
    void setXY(int, int, piece*);  // βάλε ένα κομμάτι στη στήλη/γραμμή X,Y
    void clearXY(int, int);        // σβήσε το κομμάτι στη στήλη/γραμμή X,Y
    void print();
    void save(std::string);        // αποθήκευση σε δυαδικό αρχείο στο δίσκο
    bool load(std::string);        // φόρτωση από δυαδικό αρχείο στο δίσκο
    string FEN();                  // μετατροπή σε συμβολοσειρά Forsyth–Edwards Notation
    void FENinit(string);          // αρχικοποίηση από συμβολοσειρά Forsyth–Edwards Notation
    void move(int fromX, int fromY, int toX, int toY, char newType);
                                   // μετακίνηση κομματιού (και πιθανή προαγωγή  του σε άλλον τύπο)

    bool blockedWay(int fromX, int fromY, int toX, int toY);
        // επιστρέφει true αν υπάρχουν κατειλημμένα τετράγωνα μεταξύ αρχικής και τελικής θέσης 

    bool vulnerableKing(char kcolor);
        // επιστρέφει true αν ο βασιλιάς kcolor χρώματος είναι εκτεθειμένος

    bool vulnerableSquare(char pcolor, int X, int Y);
        // επιστρέφει true αν ένας παίκτης kcolor χρώματος είναι εκτεθειμένος στο τετράγωνο X,Y

    bool available_moves(char pcolor);
        // υπάρχουν νόμιμες κινήσεις για τον παίκτη χρώματος pcolor;

    void clone(board);		// δημιουργία αντιγράφου
};

#endif
