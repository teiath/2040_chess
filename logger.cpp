#include <fstream>
#include <string>
#include <ctime>		// date/time

#include "exceptions.hpp"
#include "logger.hpp"
using namespace std;


void
 logger::log(string s)
{
  time_t now = time(0);		// η ημερομηνία/ώρα του τρέχοντος συστήματος
  tm *ltm = localtime(&now);	// η ημερομηνία/ώρα στα επιμέρους συστατικά της

  if (logstream) {

    try {
      // προσθήκη χρονοσήμαντρου
      string t1, t2;
      t1 = ((1 + ltm->tm_mon) < 10) ? "0" : "";
      t2 = ((1 + ltm->tm_mday) < 10) ? "0" : "";
      logstream << "# " << 1900 + ltm->tm_year
	  << "-" << t1 << ltm->tm_mon
	  << "-" << t2 << ltm->tm_mday
	  << " " << ltm->tm_hour << ":"
	  << ltm->tm_min << ":" << ltm->tm_sec << endl
	  // προσθήκη εγγραφής
	  << s << endl;
      if (!logstream.good())
	throw myEx(55);		// Δεν μπορώ να γράψω στο αρχείο
    }
    catch(myEx & E) {
      E.warning(s);
    }
  }
}

logger::~logger(void)
{
  if (logstream)
    logstream.close();
}

logger::logger()
{
  // nothing
}

bool logger::lopen(char *filename)
{
  try {
    logstream.open(filename, std::ios::out);
    if (logstream.fail())
      throw myEx(53);		// Δεν μπορώ να ανοίξω το αρχείο
    return true;
  }
  catch(myEx & E) {
    E.warning("καταγραφής κινήσεων " + (string) filename);
  }
  return false;
}


#ifdef TEST_UNIT
int main()
{
  logger K, L;
  K.lopen((char *)"/nosuchfile/a.log");
  L.lopen((char *)"/tmp/a.log");

  L.log("μήνυμα 1 2 3 όλα ωραία & καλά!");
  return 0;
}
#endif				/* TEST_UNIT */
