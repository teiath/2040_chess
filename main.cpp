#include <cstdio>		// printf()
#include <string.h>		// memcpy()
#include <iostream>
#include <string>
#include "common.hpp"
#include "logger.hpp"
#include "board.hpp"
#include "showMessage.hpp"
#include "exceptions.hpp"


#include "piece.hpp"
int
 piece::counter;		// αυτόματη αρχικοποίηση σε 0 (static int στην κλάση piece)

using namespace std;
int
game_input(string * inputstr, string * extraarg,
	   int *fromX, int *fromY, char *fromType,
	   int *toX, int *toY, char *toType);

bool
sourceSquareHeuristics(board & b, int *fromX, int *fromY,
		       char fromColor, char fromType, int toX, int toY,
		       char toType, int move_type,
		       bool check_vulnerable_king);

int main(int argc, char *argv[])
{
  int fromX, fromY, toX, toY;
  char fromType, toType;
  int desired_move, arg = 1;
  char player = WHITE;
  int move_no = 1, move_error_code;
  string inputstr, s, notice;
  piece *piece_to_move = NULL;
  bool end_of_game = false, performedCastling = false;
  bool logging = false;		// να γίνεται καταγραφή των κινήσεων των παικτών;
  logger L;
  board B;


  if (argc == 3) {		// δόθηκαν παράμετροι στο πρόγραμμα;
    if ((string) argv[1] == "--logfile") {
      logging = L.lopen(argv[2]);
    }
  }
  // Ξεκίνα με μια τυπική σκακιέρα
  B.FENinit((string) "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR");
  B.print();




  while (!end_of_game) {

    cout << endl << (int)((move_no + 1) / 2) << (string) ". " <<
	(string) ((player == WHITE) ? "(λευκά)" : "(μαύρα)") <<
	(string)
	"  Εισάγετε κίνηση ή \"exit\" για έξοδο ";

    notice = "";
    try {

      // πάρε την είσοδο του χρήστη, έλεγξέ την και μετάφρασέ την σε ενέργεια
      desired_move =
	  game_input(&inputstr, &s, &fromX, &fromY, &fromType, &toX, &toY,
		     &toType);

#ifdef DEBUG
	if (desired_move > MOVE_ERROR) {
	  printf("readmove1 <%s>: return value:%d\n", inputstr.c_str(),
		 desired_move);
	} else {
	  printf
	      ("readmove2 <%s>: <%c %c%d --> %c %c%d>\tstring:%s return value:%d\n",
	       inputstr.c_str(), fromType, COLN2A(fromX), fromY, toType,
	       COLN2A(toX), toY, s.c_str(), desired_move);
	}
#endif


      if (desired_move == MOVE_WINW) {
	notice = "";
	end_of_game = true;
	throw myEx(61);		// Τέλος παιγνιδιού. Τα λευκά κερδίζουν.
      }
      if (desired_move == MOVE_WINB) {
	notice = "";
	end_of_game = true;
	throw myEx(62);		// Τέλος παιγνιδιού. Τα μαύρα κερδίζουν.
      }

      if (desired_move == MOVE_DRAW) {
	notice = "";
	end_of_game = true;
	throw myEx(63);		// Τέλος παιγνιδιού. Ισοπαλία.
      }

      if (desired_move == MOVE_QUIT) {
	break;
      }

      if (desired_move == MOVE_COMMENT) {
	continue;
      }

      if (desired_move == MOVE_ERROR) {
	notice = inputstr;
	throw myEx(56);		// Μη έγκυρος αλγεβρικός συμβολισμός κίνησης στην είσοδο
      }

      if (desired_move == MOVE_INIT) {
	B.FENinit(s);
	B.print();
	goto next_move;
      }


      if (desired_move == MOVE_SAVE) {
	if (player != WHITE) {
	  notice = "";
	  throw myEx(57);	// Αποθήκευση σε αρχείο επιτρέπεται μόνο πριν από κίνηση των λευκών
	} else {
	  B.save(s);
	  goto next_move;
	}
      }

      if (desired_move == MOVE_LOAD) {
	if (B.load(s))
	  B.print();
	// Μετά από ανάγνωση ταμπλό από αρχείο είναι σειρά των λευκών να παίξουν 
	move_no = 1;
	player = WHITE;
	goto next_move;
      };



      if ((desired_move & MOVE_OO) || (desired_move & MOVE_OOO)) {  // ροκέ
	cout << "doing castling 10 desired_move: " << desired_move << endl;
	// αρχικές και τελικές θέσεις
	int kingX1, rookX1, kingX2, rookX2, Y;
	if (desired_move & MOVE_OO) {	// μικρό ροκέ
	  kingX1 = 5;
	  rookX1 = 8;
	  kingX2 = 7;
	  rookX2 = 6;
	} else {		// μεγάλο ροκέ
	  kingX1 = 5;
	  rookX1 = 1;
	  kingX2 = 3;
	  rookX2 = 4;
	}
	Y = (player == WHITE) ? 1 : 8;
	piece *pk = B.getXY(kingX1, Y);
	piece *pr = B.getXY(rookX1, Y);
	if (pk==NULL || pr==NULL) {
	  throw myEx(25);	// Στο ροκέ πρέπει βασιλιάς και πύργος να μην έχουν μετακινηθεί
	}


	if ((pk->getType() != KING) || (pr->getType() != ROOK) ||
	    (pk->getState() != STATE_INITIAL)
	    || (pr->getState() != STATE_INITIAL) || (pk->getColor() != player)
	    || (pr->getColor() != player)) {
	  throw myEx(25);	// Στο ροκέ πρέπει βασιλιάς και πύργος να μην έχουν μετακινηθεί
	}
	if (B.blockedWay(kingX1, Y, rookX1, Y)) {
	  throw myEx(26);	// Στο ροκέ τα τετράγωνα μεταξύ βασιλιά και πύργου πρέπει να είναι κενά
	}
	if (B.vulnerableKing(player)) {
	  throw myEx(27);	// Το ροκέ απαγορεύεται όταν ο βασιλιάς απειλείται
	}
	if ((B.vulnerableSquare(player, kingX2, Y)) ||
	    (B.vulnerableSquare(player, rookX2, Y))) {
	  throw myEx(28);	// Το ροκέ απαγορεύεται όταν τα τετράγωνα από τα οποία θα περάσει ο βασιλιάς απειλούνται
	}
	// Δημιούργησε ένα αντίγραφο της τρέχουσας σκακιέρας, εκτέλεσε
	// τις κινήσεις του ροκέ και έλεγξε αν ο βασιλιάς απειλείται. Αν ναι,
	// το ροκέ δεν επιτρέπεται
	board C;
	piece pktmp, prtmp;	// πάρε ένα τοπικό αντίγραφο των κομματιών
	C.clone(B);		// αντίγραψε την κυρία σκακιέρα σε μια βοηθητική
	C.getXY(kingX1,Y)->setState(STATE_MOVED);
	C.getXY(rookX1,Y)->setState(STATE_MOVED);
	C.move(kingX1, Y, kingX2, Y, KING);
	C.move(rookX1, Y, rookX2, Y, ROOK);
	if (C.vulnerableKing(player)) {
	  throw myEx(29);	// Το ροκέ απαγορεύεται όταν ο βασιλιάς στην νέα θέση του απειλείται
	} else {		// αφού όλα είναι εντάξει, αντίγραψε την βοηθητική σκακιέρα στην κυρία
	  B.getXY(kingX1,Y)->setState(STATE_MOVED);
	  B.getXY(rookX1,Y)->setState(STATE_MOVED);
	  B.move(kingX1, Y, kingX2, Y, KING);
	  B.move(rookX1, Y, rookX2, Y, ROOK);
	  // δώσε έγκυρες τιμές σε μερικές μεταβλητές που ελέγχονται παρακάτω
	  fromX = 1;
	  fromY = 1;
	  toX = 1;
	  toY = 1;
	  performedCastling = true;
	}
      } else {
	  performedCastling = false;
      }

#ifdef DEBUG
      printf("after OO checking: fromX=%d; fromY=%d; toX=%d; toY=%d\n", fromX, fromY, toX, toY);
#endif

      // Μήπως η κίνηση που εισήχθη δεν καθορίζει το τετράγωνο-προορισμός;
      // Κανονικά δεν πρέπει να συμβαίνει, αλλά ας είμαστε διπλά σίγουροι
      if ((!validX(toX)) || (!validY(toY))) {
	notice = inputstr;
	throw myEx(58);		// Μη έγκυρη είσοδος, δεν καθορίζεται το τετράγωνο-προορισμός
      }


      // Μήπως η κίνηση που εισήχθη δεν καθορίζει ρητώς το τετράγωνο-αφετηρία;
      // Τότε θα πρέπει να το μαντέψουμε, ελέγχοντας τα διαθέσιμα κομμάτια
      // χρησιμοποιώντας ευριστικές μεθόδους
      if ((!validX(fromX)) || (!validY(fromY))) {
	int k = sourceSquareHeuristics(B, &fromX, &fromY, player, fromType,
				       toX, toY, toType, desired_move, true);
	if (k == 0) {
	  notice = inputstr;
	  throw myEx(59);	// Μη έγκυρη είσοδος, δεν υπάρχει κομμάτι που να μπορεί να κάνει αυτή την κίνηση
	}
	if (k > 1) {
	  notice = inputstr;
	  throw myEx(60);	// Μη έγκυρη είσοδος, δεν καθορίζεται μονοσήμαντα το τετράγωνο-αφετηρία
	}
	if (!((k == 1) && (validX(fromX)) && (validY(fromY)))) {
	  // Τελικώς δεν καταφέραμε να το μαντέψουμε το τετράγωνο-αφετηρία
	  notice = inputstr;
	  throw myEx(60);	// Μη έγκυρη είσοδος, δεν καθορίζεται μονοσήμαντα το τετράγωνο-αφετηρία
	}
      }


#ifdef DEBUG
      printf
	  ("readmove<%s>: <%c %c%d-->%c %c%d>\tstring:<%s> return value:%d\n",
	   inputstr.c_str(), fromType, COLN2A(fromX), fromY, toType,
	   COLN2A(toX), toY, s.c_str(), desired_move);
#endif

      int ret_code;

      if (!performedCastling) {
	piece_to_move = B.getXY(fromX, fromY);
	if (piece_to_move) {
	  ret_code =
	      piece_to_move->can_move(B, toX, toY, toType, desired_move,
				      true);
	  if (ret_code != 0) {
	    throw myEx(ret_code);	// δείξε τι πρόβλημα προέκυψε
	  }
	  // όλα καλά, αποδεκτή κίνηση, εκτέλεσέ την
	  if (desired_move & MOVE_CAPTURE)	// αιχμαλωσία αντιπάλου;
	  {
	    B.clearXY(toX, toY);
	  }
          B.move(fromX, fromY, toX, toY, toType);
	}
      }


      // εμφάνισε την τρέχουσα κίνηση στην ιστοσελίδα
      int pad, move_tmp = ((move_no + 1) / 2);
      string tstr = "";
      if (player == WHITE) {
	tstr += "|";		// newline delimiter
	tstr += to_string(move_tmp);
      }
      tstr += (player == WHITE) ? ". " : "  ";
      tstr += inputstr;
      for (pad = 0; pad < 9 - inputstr.size(); pad++) tstr += " ";
      showMessage("", "", tstr);


      // κατάγραψε την κίνηση, αν έχει ζητηθεί καταγραφή κινήσεων
      if (logging) {
	L.log(inputstr);
      }

#ifdef DEBUG
      printf("main 82 desired_move:%d  check & MOVE MOVE:%d   available_moves(%c)=%d\n", desired_move, (desired_move & MOVE_MOVE), player, B.available_moves(player));
#endif

      if (desired_move & MOVE_MOVE) {	// ένα τουλάχιστον κομμάτι μετακινήθηκε
	B.print();		// εμφάνισε την αλλαγμένη σκακιέρα

	move_no++;
	player = nextPlayer(player);	// εναλλαγή παίκτη που έχει σειρά να παίξει



	if (B.vulnerableKing(player)) {
	  if (!(B.available_moves(player))) {
	    desired_move |= MOVE_CHECKMATE;
	  } else {
	    desired_move |= MOVE_CHECK;
	  }
	}
      }



      if (desired_move & MOVE_CHECKMATE) {	// τέλος παιγνιδιού
	if (player == BLACK) {	// (έχει προηγηθεί ήδη εναλλαγή παίκτη)
	  end_of_game = true;
	  notice = "";
	  throw myEx(61);	// Τέλος παιγνιδιού. Τα λευκά κερδίζουν.
	} else {
	  end_of_game = true;
	  notice = "";
	  throw myEx(62);	// Τέλος παιγνιδιού. Τα μαύρα κερδίζουν.
	}
	break;
      };


      // αν ένας παίκτης δεν έχει νόμιμες κινήσεις αλλά ο βασιλιάς του
      // δεν είναι σαχ, έχουμε ισοπαλία τύπου stalemate
      // (έχει προηγηθεί ήδη εναλλαγή παίκτη)
      if ((!(desired_move & MOVE_CHECK)) && (!(B.available_moves(player)))) {
	end_of_game = true;
	notice = "";
	throw myEx(64);		// Τέλος παιγνιδιού. Ισοπαλία stalemate.
	break;
      }


    }
    catch(myEx & E) {
      E.warning(notice);
    };



  next_move:
    ;
  }
}
