// Γενικές παράμετροι

#define ROWS 8
#define COLS 8


#define MOVE_NONE	0x0
#define MOVE_MOVE	0x1
#define MOVE_CAPTURE	0x2
#define MOVE_PROMOTE	0x4
#define MOVE_OO		0x8
#define MOVE_OOO	0x10 
#define MOVE_CHECK	0x20
#define MOVE_CHECKMATE	0x40
#define MOVE_ERROR	0x80
#define MOVE_COMMENT	0x100
#define MOVE_QUIT 	0x101
#define MOVE_WINW 	0x102
#define MOVE_WINB 	0x103
#define MOVE_DRAW 	0x104
#define MOVE_LOAD 	0x105
#define MOVE_SAVE 	0x106
#define MOVE_INIT 	0x107


#define KING     'K'		// βασιλιάς
#define QUEEN    'Q'		// βασίλισσα
#define ROOK     'R'		// πύργος
#define BISHOP   'B'		// αξιωματικός
#define KNIGHT   'N'		// ίππος
#define KNIGH2   'S'		// ίππος (διαφορετική γραφή)
#define PAWN     'P'		// πιόνι
#define CAPTURE  'x'		// ένδειξη αιχμαλωσίας
#define CAPTUR2  ':'		// ένδειξη αιχμαλωσίας (διαφορετική γραφή)
#define PROMOTE  '='		// ένδειξη προαγωγής πιονιού
#define CASTLING_DELIMITER '-'  // (προαιρετικό) διαχωριστικό O στα ροκέ
#define CASTLING 'O'		// ένδειξη ροκέ
#define CASTLIN2 '0'		// ένδειξη ροκέ (διαφορετική γραφή)
#define CHECK    '+'		// επίθεμα δήλωσης σαχ
#define CHECKMATE '#'		// επίθεμα δήλωσης ρουά ματ


// -: άδεια θέση    W: λευκό κομμάτι    B: μαύρο κομμάτι
#define WHITE   'W'
#define BLACK   'B'
#define BLANK   '-'

#define STATE_INITIAL  0		// αρχική κατάσταση κομματιού
#define STATE_MOVED    1		// το κομμάτι έχει μετακινηθεί

// returns 1 if valid, or 0 if invalid	(int)
#define validState(c)	\
	(((c) == STATE_INITIAL) || ((c) == STATE_MOVED))


#define toUpperCase(c)	((c) + 'A' - 'a')
#define toLowerCase(c)	((c) + 'a' - 'A')
#define isUpperCase(c)	(((c) >= 'A') && ((c) <= 'Z'))
#define isLowerCase(c)	(((c) >= 'a') && ((c) <= 'z'))


// returns 1 if valid, or 0 if invalid	(int)
#define validType(c)	\
	(((c) == KING) || ((c) == QUEEN) || \
	((c) == BISHOP) || ((c) == ROOK) || ((c) == KNIGHT) || ((c) == PAWN))
// valid chars: white pieces (capital letters) or black pieces (small letters)
#define validChar(c)	\
	( validType(c) || validType(toLowerCase(c))

// returns 1 if valid, or 0 if invalid	(int)
#define validColor(c)	\
	(((c) == WHITE) || ((c) == BLACK))

// returns 1 if valid, or 0 if invalid	(int)
#define validX(x)	((x)>=1 && (x)<=COLS)
#define validY(y)	((y)>=1 && (y)<=ROWS)

// alternating players
#define nextPlayer(c)	(((c) == WHITE) ? BLACK : WHITE)

// convert numeric column number to column character
#define COLN2A(x)		((validX(x)?(x)+'a'-1:'0'))


#define ABS(x)		(((x)>0) ? (x) : (-(x)) )	// απόλυτη τιμή
//#define MIN(x,y)	(((x)>(y)) ? (y) : (x) )
//#define MAX(x,y)	(((x)<(y)) ? (y) : (x) )
