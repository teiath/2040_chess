#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <string>
using namespace std;

class myEx
{
private:
  int code;
public:
  myEx ();
  myEx (int c);
  string String();
  void error(string);
  void warning(string);
};
#endif
