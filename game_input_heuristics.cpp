// Aν η κίνηση που εισήχθη από τον χρήστη δεν καθορίζει το τετράγωνο-αφετηρία
// τότε θα πρέπει να το μαντέψουμε, ελέγχοντας τα διαθέσιμα κομμάτια
// χρησιμοποιώντας ευριστική μέθοδο.
// Επιστρέφει το πλήθος των κομματιών (0, 1, ...) που θα μπορούσαν να
// κινηθούν προς το τετράγωνο-προορισμός

#include "common.hpp"
#include "piece.hpp"
#include "board.hpp"

#ifdef DEBUG
#include <cstdio>
#endif

int
sourceSquareHeuristics (board & b, int *fromX, int *fromY,
			char fromColor, char fromType,
			int toX, int toY, char toType, int move_type, bool check_vulnerable_king)
{

#ifdef DEBUG
  printf("entering sourceSquareHeuristics fromColor:%c fromType:%c  to:%c%d to_type:%c move_type:%d check_vulnerable_king:%d\n",fromColor, fromType,COLN2A(toX),toY, toType,move_type,check_vulnerable_king);
#endif
  int x, y, retval, potential_X, potential_Y;
  piece *p;
  int potential_pieces = 0;	// πόσα κομμάτια μπορούν να εκτελέσουν αυτή την κίνηση

  if (!(validColor(fromColor))) return 0;
  if (!(validType(fromType))) return 0;

  for (x = 1; x <= COLS; x++)
    for (y = 1; y <= ROWS; y++)
      {
	if (validX (*fromX) && (x != (*fromX)))
	  continue;		// doesn't match given x
	if (validY (*fromY) && (y != (*fromY)))
	  continue;		// doesn't match given y
	p = b.getXY (x, y);
	if (p == NULL)
	  continue;
	if (p->getColor()!=fromColor)	// doesn't match given color
	  continue;
	if (p->getType()!=fromType)	// doesn't match given type
	  continue;


	retval = p->can_move (b, toX, toY, toType, move_type, check_vulnerable_king);
#ifdef DEBUG
        printf ("sourceSquareHeuristics:  can%s move from %c%d to %c%d (reason %d) check_vuLnking=%d\n", (retval == 0 ? "" : "not"), COLN2A(x), y, COLN2A(toX), toY, retval, check_vulnerable_king);
#endif
	if (retval == 0)
	  {			// όλοι οι έλεγχοι επιτυχείς
	    potential_pieces++;
	    potential_X = x;
	    potential_Y = y;
	  }
	}

  // βρέθηκε ακριβώς ένα κομμάτι που μπορεί να παίξει την κίνηση;
  if (potential_pieces == 1)
    {
      *fromX = potential_X;
      *fromY = potential_Y;
#ifdef DEBUG
      printf ("sourceSquareHeuristics 50 βρέθηκε ΕΝΑ ΚΟΜΜΑΤΙ: %c%d\n", COLN2A(potential_X), potential_Y);
#endif
    }

  return potential_pieces;
}
