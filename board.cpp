#include <cstdio>		// printf()
#include <fstream>		// write()
#include <string.h>		// memcpy()

#include "common.hpp"
#include "exceptions.hpp"
#include "showMessage.hpp"
#include "board.hpp"
#include "piece.hpp"
using namespace std;



void
board::clear ()
{
  int x, y;
  for (x = 1; x <= COLS; x++)
    for (y = 1; y <= ROWS; y++)
      {
	if (b[y - 1][x - 1] != NULL)
	  delete b[y - 1][x - 1];	// delete old piece
	b[y - 1][x - 1] = (piece *) NULL;
      }
}

board::board (void)
{
  // αρχικώς όλες οι θέσεις της σκακιέρας είναι κενές
  int x, y;
  for (x = 1; x <= COLS; x++)
    for (y = 1; y <= ROWS; y++)
      b[y - 1][x - 1] = (piece *) NULL;
}


piece *
board::getXY (int X, int Y)
{
  try
  {
    if (!validX (X) || !validY (Y))
      {
	throw myEx (1);		// Μη έγκυρη γραμμή ή στήλη
      }
  }
  catch (myEx & E)
  {
    E.error ("");
  }
  return b[ROWS - Y][X - 1];	// οι γραμμές αυξάνουν από κάτω προς τα επάνω
}


void
board::setXY (int X, int Y, piece * p)
{
  piece *n;
  try
  {
    if (!validX (X) || !validY (Y))
      {
	throw myEx (1);		// Μη έγκυρη γραμμή ή στήλη
      }
    if (p == NULL)
      {
	throw myEx (9);		// Μη έγκυρο κομμάτι προς τοποθέτηση στη σκακιέρα
      }
    n = new piece;
    if (n == NULL)
      throw myEx (52);		// Δεν μπορώ να δημιουργήσω νέο κομμάτι στην μνήμη
    memcpy (n, p, sizeof (piece));
    b[ROWS - Y][X - 1] = n;
  }
  catch (myEx & E)
  {
    E.error ("");
  }
}


void
board::clearXY (int X, int Y)
{
  try
  {
    if (!validX (X) || !validY (Y))
      {
	throw myEx (1);		// Μη έγκυρη γραμμή ή στήλη
      }
    // σβήσε από την μνήμη το πιόνι που τυχόν υπάρχει στη συγκεκριμένη θέση
    if (b[ROWS - Y][X - 1] != NULL)
      {
	delete b[ROWS - Y][X - 1];
      }
    b[ROWS - Y][X - 1] = NULL;
  }
  catch (myEx & E)
  {
    E.warning ("");
  }
}



void
board::move (int fromX, int fromY, int toX, int toY, char newType)
{
  piece *fromP, *toP;
  try
  {
    if (!validX (fromX) || !validY (fromY) || !validX (toX) || !validY (toY))
      {
	throw myEx (1);		// Μη έγκυρη γραμμή ή στήλη
      };
    toP = getXY (toX, toY);
    fromP = getXY (fromX, fromY);
    if (fromP == NULL)
      {
	throw myEx (14);	// Μη έγκυρο κομμάτι στη σκακιέρα κατά την μετακίνηση κομματιού
      }

    if (toP != NULL)
      {
	delete toP;
      }

    fromP->setType (newType);		// update piece type
    fromP->setState (STATE_MOVED);	// update piece state
    b[ROWS - fromY][fromX - 1] = (piece *) NULL;	// μετακίνηση
    b[ROWS - toY][toX - 1] = fromP;
  }
  catch (myEx & E)
  {
    E.error ("");
  }
}



void
printOne (piece * p)
{
  char c = BLANK;
  if (p != NULL)
    {
      c = p->getType ();
      if (p->getColor () == BLACK)
	c = toLowerCase (c);
    }
  printf ("%c ", c);
}


void
board::print ()
{
  int i, j;
  for (i = ROWS; i >= 1; i--)
    {
      printf (" %d  ", i);
      for (j = 1; j <= COLS; j++)
	printOne (getXY (j, i));
      printf ("\n");
    }
  printf ("\n    ");
  for (j = 1; j <= COLS; j++)
    printf ("%c ", j + 'a' - 1);
  printf ("\n\n");

  showMessage ("", FEN (), "");
}



void
board::save (std::string filename)
{

  try
  {
    std::ofstream fs (filename, std::ios::out | std::ios::binary);
    if (fs.fail ())
      throw myEx (53);		// Δεν μπορώ να ανοίξω το αρχείο

    char record[4];
    int i, j;
    char t, c;
    for (i = 1; i <= ROWS; i++)
      {
	for (j = 1; j <= COLS; j++)
	  {
	    piece *p = getXY (j, i);
	    if ((p != NULL) &&
		(p->getState () == STATE_INITIAL
		 || p->getState () == STATE_MOVED))
	      {
		t = p->getType ();
		c = p->getColor ();
		if (c == BLACK)
		  t = toLowerCase (t);
		record[0] = (unsigned char) i;
		record[1] = (unsigned char) j;
		record[2] = (unsigned char) t;
		record[3] =
		  (unsigned char) (p->getState () == STATE_MOVED ? 1 : 0);
		fs.write (record, sizeof (record));
	      }
	  }
      }
    fs.close ();
  }
  catch (myEx & E)
  {
    E.warning (filename);
  }
}


// επιστρέφει true αν το αρχείο διαβάστηκε επιτυχώς
bool
board::load (std::string filename)
{

  try
  {
    std::ifstream fs (filename, std::ios::in | std::ios::binary);
    if (fs.fail ())
      throw myEx (53);		// Δεν μπορώ να ανοίξω το αρχείο

    char record[4];
    int i, j;
    char t;

    clear ();
    piece *p;
    while (fs.read (reinterpret_cast < char *>(record), sizeof (record)))
      {
	try
	{
	  p = new piece;
	  if (p == NULL)
	    throw myEx (52);	// Δεν μπορώ να δημιουργήσω νέο κομμάτι στην μνήμη
	}
	catch (myEx & F)
	{
	  F.error ("");
	};
	p->setState ((record[3] ==
		      (unsigned char) 1) ? STATE_MOVED : STATE_INITIAL);
	t = record[2];
	if (isLowerCase (t))
	  {
	    t = toUpperCase (t);
	    p->setColor (BLACK);
	  }
	else
	  {
	    p->setColor (WHITE);
	  }
	p->setType (t);
	setXY (record[1], record[0], p);
      }
    fs.close ();
    return true;
  }
  catch (myEx & E)
  {
    E.warning (filename);
  }
  return false;

}





string
board::FEN ()
{
  int i, j, blanks = 0;
  piece *p;
  char c;

  char s[ROWS * COLS + ROWS + 1], *S = s;
  for (i = ROWS; i >= 1; i--)
    {
      if (blanks > 0)
	{
	  *S = '0' + blanks;
	  S++;
	}
      if (i != ROWS)
	{
	  *S = '/';
	  S++;
	}
      blanks = 0;
      for (j = 1; j <= COLS; j++)
	{
	  p = getXY (j, i);
	  if (p == NULL)
	    {
	      blanks++;
	    }
	  else
	    {
	      c = p->getType ();
	      if (p->getColor () == BLACK)
		c = toLowerCase (c);
	      if (blanks > 0)
		{
		  *S = '0' + blanks;
		  S++;
		  blanks = 0;
		}
	      *S = c;
	      S++;
	    }
	}
    }
  if (blanks > 0)
    {
      *S = '0' + blanks;
      S++;
    }
  *S = '\0';
  return (string) s;
}



void
board::FENinit (string str)
{
  clear ();			// άδειασμα από τυχόν παλαιά κομμάτια
  int x = 1, y = ROWS;		// τρέχουσα στήλη/γραμμή
  string t = str;
  char col;
  piece *p;

  try
  {
    while (t != (string) "")
      {
	char c = t[0];
	if (c == '/')		// αλλαγή γραμμής, οι γραμμές μικραίνουν προς τα κάτω
	  {
	    y--;
	    if (!validY (y))
	      throw myEx (54);	// Μη έγκυρη συμβολοσειρά FEN
	    x = 1;
	    goto do_next_char;
	  }

	if ((c - '0' >= 1) && (c - '0' <= COLS))	// αλλαγή στήλης
	  {
	    x += (c - '1' + 1);
	    goto do_next_char;
	  }
	if (isUpperCase (c))
	  {
	    col = WHITE;
	  }
	if (isLowerCase (c))
	  {
	    col = BLACK;
	    c = toUpperCase (c);
	  }
	if ((!validColor (col)) || (!validType (c)))
	  throw myEx (54);	// Μη έγκυρη συμβολοσειρά FEN
	p = new piece;
	if (p == NULL)
	  throw myEx (52);	// Δεν μπορώ να δημιουργήσω νέο κομμάτι στην μνήμη
	p->setType (c);
	p->setColor (col);
	p->setState (STATE_INITIAL);
	if ((!validX (x)) || (!validY (y)))
	  throw myEx (54);	// Μη έγκυρη συμβολοσειρά FEN
	setXY (x, y, p);

	x++;
	//if (!validX(x)) throw myEx (54);      // Μη έγκυρη συμβολοσειρά FEN
      do_next_char:
	t = t.substr (1, t.size ());	// συνέχισε με τον επόμενο χαρακτήρα
      }
  }
  catch (myEx & E)
  {
    E.warning (str);
  }
}




bool
board::blockedWay (int fromX, int fromY, int toX, int toY)
{
  // πώς πρέπει να κινηθούμε για να πάμε από την αφετηρία στον προορισμό;
  int dx = (fromX < toX) ? 1 : (fromX == toX ? 0 : -1);
  int dy = (fromY < toY) ? 1 : (fromY == toY ? 0 : -1);
  int x, y;
  piece *p;

  for (x = fromX + dx, y = fromY + dy; ((x != toX) || (y != toY));
       x = x + dx, y = y + dy)
    {
      p = getXY (x, y);
      if (p != NULL)
	return true;		// Υπάρχουν κατειλημμένα τετράγωνα μεταξύ αρχικής και τελικής θέσης
    }
  return false;			// Δεν υπάρχουν κατειλημμένα τετράγωνα μεταξύ αρχικής και τελικής θέσης
}




void
board::clone (board orig)
{
  int x, y;
  piece *n, *p;

  // αρχικώς όλες οι θέσεις της σκακιέρας κλώνου πρέπει να είναι κενές
  clear ();

  try
  {
    for (x = 1; x <= COLS; x++)
      for (y = 1; y <= ROWS; y++)
	{
	  p = orig.getXY (x, y);
	  if (p)
	    {
	      n = new piece;
	      if (n == NULL)
		throw myEx (52);	// Δεν μπορώ να δημιουργήσω νέο κομμάτι στην μνήμη
	      memcpy (n, p, sizeof (piece));
	      setXY (x, y, n);
	    }
	}
  }
  catch (myEx & E)
  {
    E.error ("");
  }
}




bool
board::vulnerableKing (char kcolor)
{
  int kingX = 0, kingY = 0, x, y;
  piece *p;

  for (y = 1; y <= ROWS; y++)
    for (x = 1; x <= COLS; x++)
      {
	p = getXY (x, y);
	if (p && (p->getType () == KING) && (p->getColor () == kcolor))
	  {
	    kingX = x;
	    kingY = y;
	  }
      }
  if ((!validX (kingX)) || (!validY (kingY)))
    {
      // δεν υπάρχει βασιλιάς χρώματος kcolor στην σκακιέρα, άρα δεν είναι εκτεθειμένος
      return false;
    }

#ifdef DEBUG
  printf ("board::vulnerableKing A found %c king at %c%d\n", kcolor, COLN2A (kingX), kingY);
#endif

  // μπορεί κάποιο από τα κομμάτια αντιθέτου χρώματος να φτάσει τον βασιλιά;
  for (y = 1; y <= ROWS; y++)
    for (x = 1; x <= COLS; x++)
      {
	p = getXY (x, y);
	if (p && (p->getColor () != kcolor))
	  {
	    int retval = p->can_move (*this, kingX, kingY, p->getType (),
				      MOVE_MOVE | MOVE_CAPTURE, false);
#ifdef DEBUG
            printf ("board::vulnerableKing C %c %c at %c%d can%s attack %c king at %c%d (check_vking %d)\n", p->getColor (), p->getType (), COLN2A (x), y, (retval == 0 ? "" : "not"), kcolor, COLN2A (kingX), kingY, false);
#endif

	    if (retval == 0)
	      {
		return true;	// αυτό το κομμάτι μπορεί να επιτεθεί στον βασιλιά, άρα ο βασιλιάς κινδυνεύει
	      }
	  }
      }
  return false;			// δεν βρέθηκε κανείς που να μπορεί να επιτεθεί στον βασιλιά
}




bool
board::vulnerableSquare(char pcolor, int toX, int toY)
{
  int x, y;
  piece *p;

  // μπορεί κάποιο από τα κομμάτια αντιθέτου χρώματος να φτάσει στο X,Y;
  for (y = 1; y <= ROWS; y++)
    for (x = 1; x <= COLS; x++)
      {
	p = getXY (x, y);
	if (p && (p->getColor () != pcolor))
	  {
	    int retval = p->can_move (*this, toX, toY, p->getType(),
				      MOVE_MOVE | MOVE_CAPTURE, false);
#ifdef DEBUG
            printf ("board::vulnerableSquare  %c %c at %c%d can%s attack %c to at %c%d (check_vuln_king %d)\n", p->getColor (), p->getType (), COLN2A(x), y, (retval == 0 ? "" : "not"), pcolor, COLN2A (toX), toY, false);
#endif

	    if (retval == 0)
	      {
		return true;	// αυτό το κομμάτι μπορεί να επιτεθεί στο toX,toY
	      }
	  }
      }
  return false;			// δεν βρέθηκε κανείς που να μπορεί να επιτεθεί στο toX,toY
}




bool
board::available_moves(char pcolor)
{
  int fromX, fromY, toX, toY;
  piece *p;

  for (fromY = 1; fromY <= ROWS; fromY++)
    for (fromX = 1; fromX <= COLS; fromX++)
      {
	p = getXY (fromX, fromY);
	if (p && p->getColor () == pcolor)
	  {
	    for (toY = 1; toY <= ROWS; toY++)
	      for (toX = 1; toX <= COLS; toX++)
		{
		  // απλή μετακίνηση ή μετακίνηση με αιχμαλωσία αντιπάλου
		  if ((p->can_move (*this, toX, toY, p->getType (), MOVE_MOVE,
				   true)==0)
		      || (p->can_move (*this, toX, toY, p->getType (),
				      MOVE_MOVE | MOVE_CAPTURE, true)==0)){
#ifdef DEBUG
                    printf("board:availmoves color %c found available move from %c%d to %c%d\n", pcolor, COLN2A(fromX),fromY,COLN2A(toX),toY);
#endif
		    return true;
}
		}
	  }
      }
  return false;
}
