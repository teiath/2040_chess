#include <iostream>
#include <string>
#include <cstdlib>		// exit()
#include "showMessage.hpp"
#include "exceptions.hpp"
using namespace std;

myEx::myEx ()
{
  // nothing
}

myEx::myEx (int c)
{
  code = c;
}

string myEx::String(){
  string s;
  switch (code) {
    case   0: s=""; break;  /* no error */
    case   1: s="Μη έγκυρη γραμμή ή στήλη"; break;
    case   2: s="Η τελική θέση είναι ίδια με την αρχική"; break;
    case   3: s="Η τελική θέση έχει ήδη πάνω της κομμάτι του αντιπάλου και η κίνηση δεν είναι σύλληψη αντιπάλου"; break;
    case   4: s="Υπάρχουν περισσότερα από ένα κομμάτια που μπορούν να κάνουν αυτή την κίνηση"; break;
    case   5: s="Υπάρχουν κατειλημμένα τετράγωνα μεταξύ αρχικής και τελικής θέσης (και το μετακινούμενο κομμάτι δεν είναι ίππος)"; break;
    case   6: s="Ο πύργος δεν επιτρέπεται να κάνει ροκέ αν έχει μετακινηθεί από την αρχική του θέση"; break;
    case   7: s="Λανθασμένη συμβολοσειρά αρχικοποίησης κομματιού"; break;
    case   8: s="Μη έγκυρος τύπος κομματιού"; break;
    case   9: s="Μη έγκυρο κομμάτι προς τοποθέτηση στη σκακιέρα"; break;
    case  10: s="Μη έγκυρο χρώμα κομματιού"; break;
    case  11: s="Μη έγκυρη κατάσταση κομματιού"; break;
    case  12: s="Δεν επιτρέπεται προαγωγή κομματιού που δεν είναι πιόνι"; break;
    case  13: s="Δεν επιτρέπεται προαγωγή παρά μόνο σε βασίλισσα, πύργο, αξιωματικό ή ίππο"; break;
    case  14: s="Μη έγκυρο κομμάτι στη σκακιέρα κατά την μετακίνηση κομματιού"; break;
    case  15: s="Ένα πιόνι δεν φαίνεται να ανήκει πλέον στην σκακιέρα"; break;
    case  16: s="Η τελική θέση έχει ήδη πάνω της δικό μας κομμάτι"; break;
    case  17: s="Μη επιτρεπόμενη κίνηση βασιλιά"; break;
    case  18: s="Μη επιτρεπόμενη κίνηση βασίλισσας"; break;
    case  19: s="Μη επιτρεπόμενη κίνηση πύργου"; break;
    case  20: s="Μη επιτρεπόμενη κίνηση ίππου"; break;
    case  21: s="Μη επιτρεπόμενη κίνηση αξιωματικού"; break;
    case  22: s="Μη επιτρεπόμενη κίνηση πιονιού"; break;
    case  23: s="Προαγωγή πιονιού μπορεί να γίνει μόνο στην τελική σειρά"; break;
    case  24: s="Μη επιτρεπόμενη κίνηση, θέτει σε κίνδυνο τον βασιλιά"; break;
    case  25: s="Στο ροκέ πρέπει βασιλιάς και πύργος να μην έχουν μετακινηθεί"; break;
    case  26: s="Στο ροκέ τα τετράγωνα μεταξύ βασιλιά και πύργου πρέπει να είναι κενά"; break;
    case  27: s="Το ροκέ απαγορεύεται όταν ο βασιλιάς απειλείται"; break;
    case  28: s="Το ροκέ απαγορεύεται όταν τα τετράγωνα από τα οποία θα περάσει ο βασιλιάς απειλούνται"; break;
    case  29: s="Το ροκέ απαγορεύεται όταν ο βασιλιάς στην νέα θέση του απειλείται"; break;



    case  51: s="Αποτυχία δέσμευσης μνήμης"; break;
    case  52: s="Δεν μπορώ να δημιουργήσω νέο κομμάτι στην μνήμη"; break;
    case  53: s="Δεν μπορώ να ανοίξω το αρχείο"; break;
    case  54: s="Μη έγκυρη συμβολοσειρά FEN"; break;
    case  55: s="Δεν μπορώ να γράψω στο αρχείο"; break;
    case  56: s="Μη έγκυρος αλγεβρικός συμβολισμός κίνησης στην είσοδο"; break;
    case  57: s="Αποθήκευση σε αρχείο επιτρέπεται μόνο πριν από κίνηση των λευκών"; break;
    case  58: s="Μη έγκυρη είσοδος, δεν καθορίζεται το τετράγωνο-προορισμός"; break;
    case  59: s="Μη έγκυρη είσοδος, δεν υπάρχει κομμάτι που να μπορεί να κάνει την κίνηση"; break;
    case  60: s="Μη έγκυρη είσοδος, δεν καθορίζεται μονοσήμαντα το τετράγωνο-αφετηρία"; break;
    case  61: s="Τέλος παιγνιδιού. Τα λευκά κερδίζουν."; break;
    case  62: s="Τέλος παιγνιδιού. Τα μαύρα κερδίζουν."; break;
    case  63: s="Τέλος παιγνιδιού. Ισοπαλία."; break;
    case  64: s="Τέλος παιγνιδιού. Ισοπαλία stalemate."; break;

    default:  s="Άγνωστο σφάλμα"; break;
  }
  return s;
}


#define	myExShow(S) {\
  string t = (S) + (string)" " +String();\
  if (extrainfo != "") t = t + " " + (extrainfo);\
  showMessage(t,(string)"",(string)"");\
  cout << endl;\
}

// severe error; show it and stop execution
void myEx::error(string extrainfo=""){
  myExShow("ΣΦΑΛΜΑ");
  exit (1);
}

// minor error; show it and keep going
void myEx::warning(string extrainfo=""){
  myExShow("");
}

