#ifndef PIECE_H
#define PIECE_H

#include "board.hpp"
#include "common.hpp"

class piece
{
private:
  int id;			// μοναδικός αριθμός ταυτότητας του κομματιού
				// για να διακρίνεται από όλα τα υπόλοιπα
  char type;			// είδος: βασιλιάς, πιόνι, ...
  char color;			// χρώμα: λευκό/μαύρο
  int state;			// κατάσταση: έχει κινηθεί;

protected:			// αριθμός παραχθέντων κομματιών
  static int counter;		// (πρέπει να δηλωθεί εξωτερικά)

public:
    piece ();
    piece (char t, char c, int s);
  char getType ();
  void setType (char);
  char getColor ();
  void setColor (char);
  int getState ();
  void setState (int);
  int getId ();


// Ελέγχει αν ένα πιόνι μπορεί να μεταβεί σε νέα θέση της σκακιέρας.
// Επιστρέφει 0 (αν οι έλεγχοι είναι επιτυχείς, ή τον κωδικό του κανόνα που
// παραβιάζεται)
  int can_move (board & b, int to_x, int to_y, char toType, int move_type, bool check_vulnerable_king);

};
#endif
