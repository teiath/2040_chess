#include "common.hpp"
#include "piece.hpp"
#include "exceptions.hpp"

#include <iostream>		// FIXME delete + cout

piece::piece() : id(piece::counter++) {
    type='?'; color='?'; state=STATE_INITIAL;
}

piece::piece (char t, char c, int s) : id(piece::counter++) {
  try {

    if (!validType(t)) {
      throw myEx( 8 ); // Μη έγκυρος τύπος κομματιού
    }
    if (!validColor(c)){
      throw myEx( 10 ); // Μη έγκυρο χρώμα κομματιού
    }
    if (!validState(s)){
      throw myEx( 11 ); // Μη έγκυρη κατάσταση κομματιού
    }
    type=t; color=c; state=s;
  } catch (myEx &E){
      E.error("");
    }

}

char
piece::getType(){ return type; }
void
piece::setType(char T){ type = T; }

char
piece::getColor(){ return color; }
void
piece::setColor(char c){ color = c; }

int
piece::getState(){ return state; }
void
piece::setState(int S){ state = S; }

int
piece::getId(){ return id; }


// Ελέγχει αν ένα πιόνι μπορεί να μεταβεί σε νέα θέση της σκακιέρας
// Επιστρέφει 0 (αν οι έλεγχοι είναι επιτυχείς, ή τον κωδικό του κανόνα που
// παραβιάζεται
int
piece::can_move(board &b, int toX, int toY, char toType, int move_type,
                bool check_vulnerable_king){


///------------------------------------- ΓΕΝΙΚΟΙ ΕΛΕΓΧΟΙ ΓΙΑ ΟΛΑ ΤΑ ΚΟΜΜΑΤΙΑ
  // ποιά είναι η θέση αυτού του πιονιού στην συγκεκριμένη σκακιέρα;
  int fromX = 0; int fromY = 0;
  int x, y; piece *p, *toPiece;

    for (y=1;y<=COLS;y++) for(x=1;x<=ROWS;x++){
      p = b.getXY(x,y);
      if (p){
      if (p->getId() == id) {
          fromX=x; fromY=y;
      }
     }
    }

    if (fromX==0 || fromY==0) // δεν βρήκα τον εαυτό μου στην σκακιέρα
        return( 15 ); // Ένα πιόνι δεν φαίνεται να ανήκει πλέον στην σκακιέρα"
    


  // τι υπάρχει στο τετράγωνο-προορισμό;
  toPiece = b.getXY(toX, toY);




    if (!validX(toX) || !validX(toY)) {
      return( 1 ); // Μη έγκυρη γραμμή ή στήλη
    }

#ifdef DEBUG
    printf("piece::can_move 50 from %c%d to %c%d\n", COLN2A(fromX),fromY,COLN2A(toX),toY);
#endif

    if (!validType(toType)){
      return( 8); // Μη έγκυρος τύπος κομματιού
    }


    if (fromX == toX && fromY == toY) {
      return( 2 ); // Η τελική θέση είναι ίδια με την αρχική
    }

    // new square already occupied by us?
    if (toPiece && (toPiece->getColor() == color)) {
      return(16); // Η τελική θέση έχει ήδη πάνω της δικό μας κομμάτι
    }


    // move to square occupied by opponent and not a capture?
    if (toPiece && (toPiece->getColor() != color) && (!(move_type & MOVE_CAPTURE))) {
      return( 3); // Η τελική θέση έχει ήδη πάνω της κομμάτι του αντιπάλου και η κίνηση δεν είναι σύλληψη αντιπάλου
    }


    // προαγωγή
    if ((move_type & MOVE_PROMOTE) && (type != PAWN)) {
      return(12); // Δεν επιτρέπεται προαγωγή κομματιού που δεν είναι πιόνι
    }
    if ((move_type & MOVE_PROMOTE) &&
	(!  (toType == QUEEN || toType == BISHOP ||
             toType == ROOK  || toType == KNIGHT)   ) ) {
      return(13); // Δεν επιτρέπεται προαγωγή παρά μόνο σε βασίλισσα, πύργο, αξιωματικό ή ίππο
    }



///------------------------------------- ΕΙΔΙΚΟΙ ΕΛΕΓΧΟΙ ΑΝΑ ΕΙΔΟΣ ΚΟΜΜΑΤΙΟΥ
  
  bool valid_move = false;

#ifdef DEBUG
  printf("piece::can_move p41 from %c%d to %c%d toType %c valid_move=%d\n", COLN2A(fromX),fromY,COLN2A(toX),toY,toType,valid_move);
#endif

  if (type == KING)		// βασιλιάς
    {
      valid_move = false;
      if (fromX == toX && ABS (fromY - toY) == 1)
	valid_move = true;
      if (fromY == toY && ABS (fromX - toX) == 1)
	valid_move = true;
      if (ABS (fromY - toY) == 1 && ABS (fromX - toX) == 1)
	valid_move = true;
      if (! valid_move)
	return(17);
    }

  if (type == QUEEN)		// βασίλισσα
    {
      valid_move = false;
      if (fromX == toX || fromY == toY)
	valid_move = true;
      if (ABS (fromY - toY) == ABS (fromX - toX))
	valid_move = true;
      if (! valid_move)
	return(18);
    }

  if (type == ROOK)		// πύργος
    {
      valid_move = false;
      if (fromX == toX || fromY == toY)
	valid_move = true;
      if (! valid_move)
	return(19);
    }

  if (type == KNIGHT)		// ίππος
    {
      valid_move = false;
      if (ABS (fromY - toY) == 2 && ABS (fromX - toX) == 1)
	valid_move = true;
      if (ABS (fromY - toY) == 1 && ABS (fromX - toX) == 2)
	valid_move = true;
      if (! valid_move)
	return(20);
    }

  if (type == BISHOP)			// αξιωματικός
    {
      valid_move = false;
      if (ABS (fromY - toY) == ABS (fromX - toX))
	valid_move = true;
      if (! valid_move)
	return(21);
    }

  if (type == PAWN)			// πιόνι
    {
      int pawnInitialRow = 2;
      int diffY = toY - fromY;
      if (color == BLACK)
	{
	  pawnInitialRow = ROWS - 1;
	  diffY = (-diffY);
	}

      valid_move = false;
      // move 2 squares forward; allowed only from pawnInitialRow
      if ((diffY == 2) && (fromY == pawnInitialRow)
	  && (!(move_type & MOVE_CAPTURE)) && (fromX == toX))
	valid_move = true;

      // move 1 square forward
      if ((diffY == 1) && (!(move_type & MOVE_CAPTURE)) && (fromX == toX))
	valid_move = true;

      // move 1 square forward diagonally (capture)
      if ((diffY == 1) && ((move_type & MOVE_CAPTURE))
	  && (ABS (fromX - toX) == 1) && (ABS (fromY - toY) == 1))
	valid_move = true;

    // προαγωγή πιονιού
    if (move_type & MOVE_PROMOTE)
      {
        int mustGoToRow = ROWS;
        if (color == BLACK)
  	{
  	  mustGoToRow = 1;
  	}
        if (toY != mustGoToRow) return(23);
      }

      if (! valid_move) return(22);
    }




    // για όλα τα κομμάτια πλην του ίππου πρέπει να μην υπάρχουν άλλα κομμάτια
    // στη σκακιέρα σε όλη την διαδρομή μεταξύ αφετηρίας και προορισμού
    if (type != KNIGHT){
        if (b.blockedWay(fromX, fromY, toX, toY)) return( 5); // Υπάρχουν κατειλημμένα τετράγωνα μεταξύ αρχικής και τελικής θέσης (και το μετακινούμενο κομμάτι δεν είναι ίππος)
    }




  // Η κίνηση δεν πρέπει να αφήνει τον βασιλιά εκτεθειμένο
  //
  // Για την αποφυγή απείρων βρόγχων (αφού καλούμε την board::vulnerableKing()
  // η οποία με την σειρά της καλεί την παρούσα μέθοδο), θα πρέπει
  // η board::vulnerableKing() να αποφεύγει αυτόν το συγκεκριμένο έλεγχο,
  // το οποίο επιτυγχάνεται με χρήση της μεταβλητής check_vulnerable_king.
  if (check_vulnerable_king){
     // - δημιούργησε ένα αντίγραφο της σκακιέρας γεμίζοντάς την με νέα κομμάτια
     // - κάνε την ελεγχόμενη κίνηση
     // - έλεγξε αν ο βασιλιάς είναι τρωτός
     board bcopy;
     bcopy.clone(b);
     bcopy.move(fromX, fromY, toX, toY, toType);
     bool retval = bcopy.vulnerableKing(color);
     if (retval)
       return(24);  // Μη επιτρεπόμενη κίνηση, θέτει σε κίνδυνο τον βασιλιά
  }





    // όλοι οι έλεγχοι ήταν επιτυχείς, η κίνηση είναι αποδεκτή!!!
    return (0);		// κανένα πρόβλημα

}



